package android.learning.com.yambaproject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = BootReceiver.class.getSimpleName();
    private static final long DEFAULT_INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
    private static final String INTERVAL = "Interval";
    private static final String CANCEL_OPERATION = "Cencelling reapeat operation";
    private static final String SETTINGS_OPERATION = "settings repeat operation for:";
    private static final String ON_RECEIVE = "onReceived";
    private static final String BOOT_RECEIVER = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        long interval = Long.parseLong(prefs.getString(INTERVAL, Long.toString(DEFAULT_INTERVAL)));
        PendingIntent operation = PendingIntent.getService(context, -1, new Intent(context, RefreshService.class), PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (interval == 0) {
            alarmManager.cancel(operation);
            Log.d(TAG, CANCEL_OPERATION);
        } else {
            alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, operation);
            Log.d(TAG, SETTINGS_OPERATION + interval);
        }
        Log.d(TAG, ON_RECEIVE);
        context.startService(new Intent(context, RefreshService.class));
        Log.d(BOOT_RECEIVER, ON_RECEIVE);
    }
}

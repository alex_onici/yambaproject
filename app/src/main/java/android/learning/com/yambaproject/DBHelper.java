package android.learning.com.yambaproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final String CREATE_TABLE_DB = "CREATE TABLE IF NOT EXISTS ";
    private static final String PARANTHESIS_LEFT = " (";
    private static final String PARANTHESIS_RIGHT = ") ";
    private static final String PRIMARY_KEY = " INTEGER PRIMARY KEY,";
    private static final String DROP_IF_NOT_EXISTS = "drop table if exists ";

    public DBHelper(Context context) {
        super(context, StatusContract.DB_NAME, null, StatusContract.DB_VERSION);
    }

    // Called only one first time we create the database.
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = CREATE_TABLE_DB + StatusContract.TABLE + PARANTHESIS_LEFT + StatusContract.Column.ID + PRIMARY_KEY +
                StatusContract.Column.CREATED_AT
                + " INT," +
                StatusContract.Column.USER
                + "  TEXT," +
                StatusContract.Column.MESSAGE + " TEXT"
                + PARANTHESIS_RIGHT;

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_TABLE = DROP_IF_NOT_EXISTS + StatusContract.TABLE;
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}

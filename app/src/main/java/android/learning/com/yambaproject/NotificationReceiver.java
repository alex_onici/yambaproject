package android.learning.com.yambaproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BootReceiver {
    public static final int NOTIFICATION_ID = 42;
    private static final String COUNT = "count";
    private static final String NEW_TWEET = "New Tweets!";
    private static final String HAVE_GOT = "You've got";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int count = intent.getIntExtra(COUNT, 0);
        PendingIntent operation = PendingIntent.getActivity(context, -1, new Intent(context, MainActivity.class), PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new Notification.Builder(context)
                .setContentTitle(NEW_TWEET)
                .setContentText(HAVE_GOT + count + NEW_TWEET)
                .setSmallIcon(android.R.drawable.sym_action_email)
                .setContentIntent(operation)
                .setAutoCancel(true)
                .getNotification();
        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}

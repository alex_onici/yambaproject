package android.learning.com.yambaproject;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction fragment = getSupportFragmentManager().beginTransaction();
            fragment.add(android.R.id.content, new DetailsFragment());
            fragment.commit();
        }
    }
}

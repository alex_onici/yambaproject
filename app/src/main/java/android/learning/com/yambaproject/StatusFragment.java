package android.learning.com.yambaproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.marakana.android.yamba.clientlib.YambaClient;
import com.marakana.android.yamba.clientlib.YambaClientException;

public class StatusFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "StatusFragment";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String API_ROOT = "http://yamba.newcircle.com/api";
    private static final String STUDENT = "student";
    private static final String DEFAULT = "";
    private static final String ON_CLICK = "onClicked with status: ";
    private static final String SUCCESFULY_POSED = "Succesfully posted!!!";
    private static final String FAILED_POSED = "Failed to post to yamba service";
    private static final String UPDATE_USERNAME_PASSWORD = "Please update your username and password";
    private EditText editStatus;
    private Button buttonTweet;
    private TextView textCount;
    private int defaultTextColor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater
                .inflate(R.layout.fragment_status, container, false);
        editStatus = view.findViewById(R.id.editStatus);
        buttonTweet = view.findViewById(R.id.buttonTweet);
        textCount = view.findViewById(R.id.textCount);
        buttonTweet.setOnClickListener(this);
        defaultTextColor = textCount.getTextColors().getDefaultColor();
        editStatus.addTextChangedListener(new TextWatcher() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void afterTextChanged(Editable s) {
                int count = 140 - editStatus.length();
                textCount.setText(Integer.toString(count));
                textCount.setTextColor(R.color.colorPrimaryDark);
                if (count < 10)
                    textCount.setTextColor(Color.RED);
                else
                    textCount.setTextColor(defaultTextColor);
            }

            @Override
            public void beforeTextChanged(CharSequence s,
                                          int start, int count,
                                          int after) {
            }

            @Override

            public void onTextChanged(CharSequence s,

                                      int start, int before,
                                      int count) {
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {
        String status = editStatus.getText().toString();
        Log.d(TAG, ON_CLICK + status);
        new PostTask().execute(status);
    }

    private final class PostTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(getActivity());
            String username = prefs.getString(USERNAME, DEFAULT);
            String password = prefs.getString(PASSWORD, DEFAULT);

            // Check username and password are not empty.
            if (TextUtils.isEmpty(username) ||
                    TextUtils.isEmpty(password)) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                getActivity().startActivity(intent);
                return UPDATE_USERNAME_PASSWORD;
            }
            YambaClient yambaCloud = new YambaClient(STUDENT, PASSWORD, API_ROOT);
            try {
                yambaCloud.postStatus(params[0]);
                return SUCCESFULY_POSED;
            } catch (YambaClientException e) {
                Log.e(TAG, e.getMessage());
                return FAILED_POSED;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(StatusFragment.this.getActivity(),
                    result, Toast.LENGTH_LONG).show();
        }
    }
}
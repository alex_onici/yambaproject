package android.learning.com.yambaproject;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;


public class RefreshService extends IntentService {
    private static final String TAG = RefreshService.class.getSimpleName();
    private static final String apiRoot = "http://yamba.newcircle.com/api";
    private static final String ON_CREATED = "onCreated";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String DEFAULT = "";
    private static final String UPDATE_USERNAME_PASSWORD = "Please update your username and password";
    private static final String TIMELINE = "getTimeline";
    private static final String NEW_STATUS = "android.learning.com.yambaproject.action.NEW_STATUTES";
    private static final String FAILED_TIMELINE = "Failed to fetch the timeline";
    private static final String ON_DESTROYED = "onDestroyed";
    private static final String FORMAT = "%s: %s";

    public RefreshService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, ON_CREATED);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String username = prefs.getString(USERNAME, DEFAULT);
        final String password = prefs.getString(PASSWORD, DEFAULT);
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, UPDATE_USERNAME_PASSWORD, Toast.LENGTH_LONG).show();
            return;
        }
        Log.d(TAG, ON_CREATED);
        ContentValues values = new ContentValues();
        Twitter cloud = new Twitter(username, password);
        cloud.setAPIRootUrl(apiRoot);
        try {
            List<Twitter.Status> timeline = cloud.getPublicTimeline();
            Log.d(TAG, TIMELINE);
            int count = 0;
            for (Twitter.Status status : timeline) {
                values.clear();
                values.put(StatusContract.Column.ID, status.getId());
                values.put(StatusContract.Column.USER, String.valueOf(status.getUser()));
                values.put(StatusContract.Column.MESSAGE, status.getText());
                values.put(StatusContract.Column.CREATED_AT, status.getCreatedAt().getTime());
                Uri uri = getContentResolver().insert(StatusContract.CONTENT_URI, values);
                if (uri != null) {
                    count++;
                    String s = String.format(FORMAT, status.getUser(), status.getText());
                    Log.i(TAG, s);
                }
            }
            if (count > 0) {
                sendBroadcast(new Intent(NEW_STATUS));
            }

        } catch (TwitterException e) {
            Log.e(TAG, FAILED_TIMELINE, e);
            Toast.makeText(this, FAILED_TIMELINE, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, ON_DESTROYED);
    }
}
